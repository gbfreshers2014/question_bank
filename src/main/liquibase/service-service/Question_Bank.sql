SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `gb_faqs` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `gb_faqs` ;

-- -----------------------------------------------------
-- Table `gb_faqs`.`Emp_info`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gb_faqs`.`Emp_info` ;

CREATE TABLE IF NOT EXISTS `gb_faqs`.`Emp_info` (
  `usename` VARCHAR(10) NOT NULL,
  `userid` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`userid`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `gb_faqs`.`Questions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gb_faqs`.`Questions` ;

CREATE TABLE IF NOT EXISTS `gb_faqs`.`Questions` (
  `question_id` INT NOT NULL,
  `question` VARCHAR(200) NOT NULL,
  `Que_time` DATETIME NULL,
  `Que_By` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`question_id`),
  UNIQUE INDEX `quistion_id_UNIQUE` (`question_id` ASC),
  UNIQUE INDEX `question_UNIQUE` (`question` ASC),
  INDEX `fk_Questions_Emp_info_idx` (`Que_By` ASC),
  CONSTRAINT `fk_Questions_Emp_info`
    FOREIGN KEY (`Que_By`)
    REFERENCES `gb_faqs`.`Emp_info` (`userid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1;


-- -----------------------------------------------------
-- Table `gb_faqs`.`Answer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gb_faqs`.`Answer` ;

CREATE TABLE IF NOT EXISTS `gb_faqs`.`Answer` (
  `answer_id` INT NOT NULL,
  `Ans_By` VARCHAR(10) NOT NULL,
  `Answer` VARCHAR(200) NULL,
  `Ans_time` DATETIME NULL,
  `question_id` INT NOT NULL,
  PRIMARY KEY (`answer_id`),
  UNIQUE INDEX `answer_id_UNIQUE` (`answer_id` ASC),
  INDEX `fk_Answer_Questions1_idx` (`question_id` ASC),
  CONSTRAINT `fk_Answer_Questions1`
    FOREIGN KEY (`question_id`)
    REFERENCES `gb_faqs`.`Questions` (`question_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
